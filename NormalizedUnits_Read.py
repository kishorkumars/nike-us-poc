import xlrd

# Give the location of the file 
loc = ("Size_Normalized Values.xlsx") 
  
# To open Workbook 
wb = xlrd.open_workbook(loc) 
sheet = wb.sheet_by_index(7) 
  
nrows=sheet.nrows
ncols=sheet.ncols
for col in range(0,ncols):
	if sheet.cell_value(0,col)=='':
		continue
	file="Normalized/"+''.join(sheet.cell_value(0,col).split('/')).lower().replace(" ","").replace("(","").replace(")","")
	print file
	fout=open(file,'w')
	for row in range(1,nrows):
		val= sheet.cell_value(row, col)
		if val=='':
			break
		else:
			# print val
			if "=" not in val:
				finalList=[val]
			else:
				normVal=val.split('=')[0].strip().lower()
				values=val.split('=')[1].strip("[").strip("]").split(",")
				finalList=values
				finalList=list(set(finalList))
				finalList.insert(0,normVal)
			finalList=[x.lower().strip().strip("]").strip("[").strip("(").strip(")") for x in finalList]
			fout.write(",".join(finalList))
			fout.write("\n")
	fout.close()
