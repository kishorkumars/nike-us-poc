import valueExtractionWhole as ve
import xlrd
import xlwt
import json
import re
# workbook = xlwt.Workbook(encoding = 'ascii')
# worksheet = workbook.add_sheet('SizeMatch')


# Give the location of the file 
loc = ("nords_sample_file_size.xlsx") 
  
# To open Workbook 
# wb = xlrd.open_workbook(loc) 
# sheet = wb.sheet_by_index(0)
# nrows=sheet.nrows
# nrows=6400
# ncols=sheet.ncols
# ncols=

def removeDecimal(rawString):
	rawString=rawString.strip()
	print "Here"
	print rawString,".0" in " "+rawString+" "
	if ".0" in " "+rawString+" ":
		return rawString.replace(".0","").strip()
	else:
		return rawString.strip()


def removeAscii(rawString):
	rawString=rawString.encode('utf-8').strip()
	return ''.join([i if ord(i) < 128 else ' ' for i in rawString])

def preprocessNorm(sizeList):
	normSizeList=[]
	for i in sizeList:
		normSizeList.append(re.sub('[^a-zA-Z0-9.]+', ' ', i).lower().strip())
		# normSizeList.append(' '.join(e for e in i if e.isalnum()).lower())
	return normSizeList

def xstr(s):
	'''Return '' if string is None'''
	return '' if s is None else str(s)

def mergeDict(dict1,dict2):
	mergedict=dict1
	for index,newdict in enumerate(mergedict):
		mergedict[index]['alpha'].extend(dict2[index]['alpha'])
		mergedict[index]['alpha']=list(set(mergedict[index]['alpha']))
	return mergedict

def extractNumbers(data):
	extracted=''.join([c for c in data if c in '1234567890.- ']).strip()
	extractedNumList=re.split('-| ',extracted)
	extractedNumList=list(filter(lambda a: a != '', extractedNumList))
	return extractedNumList

def match(category="default",subcategory="default",compAvailSizeText=""):
	'''
	Input: category: apparel/footwear
		   subcategory: men/women
		   compAvailSizeText: comma seperated list of size in String format
	Output:
			matchFound: True/False, if there is overlapping size present
			AllCoreSizeFound: True/False, If all core sizes are available.
			countMatchFound: int Number, Outputs how many core matches were found.
			verificationReq: True/False, If QA verification is needed.
	'''

	subcategory=subcategory.lower().strip('s')
	if category.lower()=='footwear':
		if subcategory.lower() in ['women','woman','womens']:
			srcAvailSize=['6','6.5','7','7.5','8','8.5','9','9.5']
		elif subcategory.lower() in ['men','man',"mens"]:
			srcAvailSize=['9','9.5','10','10.5','11','11.5']

	if category.lower()=='apparel':
		srcAvailSize=['s','m','l']

	compAvailSize=(removeAscii((compAvailSizeText))).replace(" or ",",").replace("-",",").replace("|",",").split(",")	
	compAvailSize=[removeDecimal(x) for x in compAvailSize]
	print compAvailSize
	matchFound,verificationReq,AllCoreSizeFound,countMatchFound=False,True,False,0
	CoreSizeFound=[]

	'''For Footwear Category we do Straight Match'''
	if category.lower()=='footwear':
		CoreSizeFound=[]
		#Rule 1: Straight Match:-If simple preprocessing gets a match.
		Norm_srcAvailSize=preprocessNorm(srcAvailSize)
		compAvailSize=preprocessNorm(compAvailSize)
		allAvailNumbers=extractNumbers(' '.join(compAvailSize))
		if any(x>30 for x in allAvailNumbers):
			verificationReq=True
			compAvailSize=[]
			for i in allAvailNumbers:
				i=float(i)
				if i>=30:
					if subcategory =='women':
						i=i-35+4
						compAvailSize.append(removeDecimal(str(round(i)).strip()))
					if subcategory=='men':
						print "men"
						i=i-37+5
						compAvailSize.append(removeDecimal(str(round(i)).strip()))
				else:
					# compAvailSize.append(str(i).strip())
					compAvailSize.append(removeDecimal(str(i)).strip())

			print "COMP SIZES: ",compAvailSize
		print "SRC SIZES: ", srcAvailSize
		for size in srcAvailSize:
			new_size=extractNumbers(size)
			# print new_size,compAvailSize,any(" "+x.lower()+" " in " "+size.lower()+" " for x in compAvailSize)
			if any(" "+x.lower()+" " in " "+size.lower()+" " for x in compAvailSize):
				matchFound,verificationReq=True,False
				CoreSizeFound.append(size)
		CoreSizeFound=list(set(CoreSizeFound))
		print "CORE SIZES FOUND: ", CoreSizeFound
		if len(set(CoreSizeFound))==len(srcAvailSize):
			AllCoreSizeFound=True
		countMatchFound=len(CoreSizeFound)
		return matchFound,AllCoreSizeFound,countMatchFound,verificationReq

	'''For Apparel Category we need both straight match and Normalization Module'''
	if category.lower()=='apparel':
		#Rule: Normalize and straight match
		compAvailSize=preprocessNorm(compAvailSize)
		Norm_srcAvailSize={}
		with open("Normalized/nikedefaultdefault","r") as fin:
			for line in fin:
				line=line.strip()
				linelist=line.split(",")
				Norm_srcAvailSize[linelist[0]]=linelist
		compAvailSize_all=[]
		
		for size in compAvailSize:
			Norm_compAvailSize=[]
			Norm_keycompAvailSize=[]
			for key in Norm_srcAvailSize:
				for keySize in Norm_srcAvailSize[key]:
					if " "+keySize.lower().strip()+" " in " "+size.lower().strip()+" ":
						Norm_compAvailSize.append(keySize.lower().strip())
						Norm_keycompAvailSize.append(key.lower().strip())
			if len(Norm_keycompAvailSize)>0:
				key=''
				for num,eachkeySize in enumerate(Norm_compAvailSize):
					if " "+size.lower().strip()+" " == " "+eachkeySize.lower().strip()+" ":
						key=Norm_keycompAvailSize[num]
						compAvailSize_all.append(key)
				if key=='':
					for n1,eachkeySize in enumerate(Norm_compAvailSize):
						if eachkeySize in compAvailSize_all:
							continue
						maxList=[]
						for n2,eachkeySize2 in enumerate(Norm_compAvailSize):
							if " "+eachkeySize+" " in " "+eachkeySize2+" ":
								maxList.append(eachkeySize2)
						keySize=max(maxList,key=len)
						key=Norm_keycompAvailSize[Norm_compAvailSize.index(keySize.lower())]
						compAvailSize_all.append(key)
		CoreSizeFound=set(srcAvailSize).intersection(set(compAvailSize_all))
		print "TEST: ",CoreSizeFound,compAvailSize_all,srcAvailSize
		compAvailSize_all=list(set(compAvailSize_all))
		if len(set(CoreSizeFound))==len(srcAvailSize):
			AllCoreSizeFound=True
		countMatchFound=len(set(CoreSizeFound))
		if int(countMatchFound) > 0:
			matchFound = True
		return matchFound,AllCoreSizeFound,countMatchFound,verificationReq

def main():	
#	inputFile='test_nike_shoes.json'
	print match("Apparel","Mens","'4X-Large','Small','Medium','Large','X-Large,XX-Large','XX-Large Tall','XXX-Large','XL-T'")



if __name__ == '__main__':
	main()

#Apparel Mens 4X-Large,Small,Medium,Large,X-Large,XX-Large,XX-Large Tall,XXX-Large,XL-T