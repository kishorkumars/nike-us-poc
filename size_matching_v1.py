import valueExtractionWhole as ve
import xlrd
import xlwt
workbook = xlwt.Workbook(encoding = 'ascii')
worksheet = workbook.add_sheet('SizeMatch')


# Give the location of the file 
loc = ("test_nords_sample_file_size.xlsx") 
  
# To open Workbook 
wb = xlrd.open_workbook(loc) 
sheet = wb.sheet_by_index(0)
nrows=sheet.nrows
nrows=3
ncols=sheet.ncols
# ncols=

def removeAscii(rawString):
	return ''.join([i if ord(i) < 128 else ' ' for i in rawString])

def preprocessNorm(sizeList):
	normSizeList=[]
	for i in sizeList:
		normSizeList.append(''.join(e for e in i if e.isalnum()).lower())
	return normSizeList

def xstr(s):
	'''Return '' if string is None'''
	return '' if s is None else str(s)

def match(prdtType,nordstromAvailSize,compAvailSize):
	matchFound,verificationReq=False,True
	#Rule 1: Straight Match:-If simple preprocessing gets a match.
	Norm_nordstromAvailSize=preprocessNorm(nordstromAvailSize)
	Norm_compAvailSize=preprocessNorm(compAvailSize)
	print "Norm exact",nordstromAvailSize,compAvailSize
	if any(x in Norm_compAvailSize for x in Norm_nordstromAvailSize):
		matchFound=True
		verificationReq=False
		return matchFound,verificationReq
	#Rule2: From normalization Module
	if matchFound==False and verificationReq==True:

		Norm_nordstromAvailSize=[]
		Norm_compAvailSize=[]
		Alpha_nord=[]
		Alpha_comp=[]
		SizeUnit_nord=[]
		SizeUnit_comp=[]
		size_nord=[]
		size_comp=[]
		unit_comp=False
		unit_nord=False
		for eachSize in nordstromAvailSize:
			Norm_nordstromAvailSize.append(ve.normalize(eachSize,prdtType))
			Alpha_nord.extend(Norm_nordstromAvailSize[-1]['alpha'])
			for num,i in enumerate(Norm_nordstromAvailSize[-1]['numeric']['number']):
				size_nord.append(i)
				SizeUnit_nord.append(i+xstr(Norm_nordstromAvailSize[-1]['numeric']['unit'][num]))
				if Norm_nordstromAvailSize[-1]['numeric']['unit'][num] is not None:
					unit_nord=True
		for eachSize in compAvailSize:
			Norm_compAvailSize.append(ve.normalize(eachSize,prdtType))
			Alpha_comp.extend(Norm_compAvailSize[-1]['alpha'])
			for num,i in enumerate(Norm_compAvailSize[-1]['numeric']['number']):
				size_comp.append(i)
				SizeUnit_comp.append(i+xstr(Norm_compAvailSize[-1]['numeric']['unit'][num]))
				if Norm_compAvailSize[-1]['numeric']['unit'][num] is not None:
					unit_comp=True
		print "Normalized: ",Norm_nordstromAvailSize,"Comp:",Norm_compAvailSize
		#Rule2: Straight match from alpha size
		if '' in Alpha_nord:
			Alpha_nord=list(set(Alpha_nord)).remove('')
		if '' in Alpha_comp:
			Alpha_comp=list(set(Alpha_comp)).remove('')
		if len(set(Alpha_nord).intersection(Alpha_comp))>1:
			matchFound,verificationReq=True,False
			return matchFound,verificationReq

		#Rule3: NumberUnit concat match
		if '' in SizeUnit_nord:
			SizeUnit_nord=list(set(SizeUnit_nord)).remove('')
		if '' in SizeUnit_comp:
			SizeUnit_comp=list(set(SizeUnit_comp)).remove('')
		try:
			if len(set(SizeUnit_nord).intersection(set(SizeUnit_comp)))>1:
				matchFound,verificationReq=True,False
				return matchFound,verificationReq
		except:
			matchFound,verificationReq=False,False
			return matchFound,verificationReq


		
		#Rule4: Only numbers match, Incase unit is missing in one 
		if (unit_comp ^ unit_nord):

			if '' in size_nord:
				size_nord=list(set(size_nord)).remove('')
			if '' in size_comp:
				size_comp=list(set(size_comp)).remove('')
			if len(set(size_nord).intersection(set(size_comp)))>1:
				matchFound,verificationReq=True,False
				return matchFound,verificationReq
		
	return matchFound,verificationReq

def main():	
	for row in range(1,nrows):
		matchFound=False
		verificationReq=True
		try:
			try:
				if str(sheet.cell_value(row,2))=="":
					nordstromAvailSize=['One Size']
				else:
					nordstromAvailSize=removeAscii(str(sheet.cell_value(row,2))).split(",")
			except:
				nordstromAvailSize=['One Size']

			try:
				if str(sheet.cell_value(row,4))=="":
					compAvailSize=['One Size']
				else:
					compAvailSize=removeAscii(str(sheet.cell_value(row,4))).split(",")
			except:
				compAvailSize=['One Size']
			matchFound,verificationReq=match("defaultdefault",nordstromAvailSize,compAvailSize)
			

			
		except Exception, e:
			print "Skipped: ",row," " ,e
		worksheet.write(row, 0, sheet.cell_value(row,0))
		worksheet.write(row, 1, sheet.cell_value(row,1))
		worksheet.write(row, 2, sheet.cell_value(row,2))
		worksheet.write(row, 3, sheet.cell_value(row,3))
		worksheet.write(row, 4, sheet.cell_value(row,4))
		worksheet.write(row, 5, sheet.cell_value(row,5))
		worksheet.write(row, 6, str(matchFound))
		worksheet.write(row, 7, str(verificationReq))

	workbook.save('Excel_SizeMatch.xls')


if __name__ == '__main__':
	main()