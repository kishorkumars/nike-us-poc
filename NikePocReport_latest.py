# from size_matching import match
import simplejson as json
import requests
import csv
import sys



# #function to get the static data, mapping based on seed SKU
# def static_data():
# 	static_mapping={}
# 	with open("NS_Report_fields.csv") as bsvfile:
# 		spamreader = csv.reader(bsvfile, delimiter=',', quotechar='"')
# 		for row in spamreader:
# 			if 'SizeDisplayName' not in row:
# 				pid=row[-1]
# 				if pid not in static_mapping:
# 					static_mapping[pid]=[row[4],row[3],row[2],row[1],row[0]]
# 		return static_mapping
#
#
# #function to write the data to csv
# def writerow(data):
#
# 	seed_sizes=data.get("seed_sizes","NA").encode('utf-8','ignore')
# 	category=data.get("category").encode("utf-8","ignore").encode("utf-8","ignore")
# 	subcategory=data.get("subcategory").encode("utf-8","ignore")
# 	brand=data.get("brand").encode("utf-8","ignore")
# 	seed_sku=data.get("seed_sku")
# 	title=data.get("bundle_name").encode("utf-8","ignore")
#
# 	avp=data.get("available_price")
# 	mrp=data.get("mrp")
#
# 	if seed_sizes!='NA':
# 		count_size=len(seed_sizes.split(','))
# 	else:
# 		count_size='NA'
#
#
# 	for s in data.get("source_list"):
#
# 		static=mapping.get(seed_sku)
#
# 		towrite=static+[seed_sku,brand,category,subcategory,title,avp,seed_sizes,count_size]
#
# 		comp_sizes=data.get(s+"_allsizes","NA").encode('utf-8','ignore')
#
#
# 		if comp_sizes!="NA" or seed_sizes!="NA": #only calling the semantics module if there is valid size in either of seed or comp
# 			try:
# 				k,j=match(category,subcategory,seed_sizes,comp_sizes) #calling semantics API for checking the size match
# 			except:
# 				k='NA'
# 				j='NA'
# 		else:
# 			k='NA'
# 			j='NA'
#
# 		towrite.append(s)
# 		towrite.append(data.get(s+"_url","").encode("utf-8","ignore"))
#
# 		if s in data:
# 			price=data.get(s)
# 			if s!='Amazon-US':
# 				price=price[0]
# 				comp_available_price=price #same as price
# 			else:
# 				price.sort()
# 				lowest=price[0]
# 				highest=price[-1]
#
# 				price=lowest
# 				comp_available_price=str(lowest)+"-"+str(highest)
# 		else:
# 			price="NA"
# 			comp_available_price="NA"
#
# 		towrite.append(price)
# 		if avp=="NA" or price=="NA":
# 			price_off_nord="NA"
# 			price_off_nord_range="NA"
# 		else:
# 			print avp
# 			print price
#
# 			price_off_nord=round(float((price-avp)/avp),2)
#
# 			if s!='Amazon-US':
# 				price_off_nord_range=price_off_nord #both are same
# 			else:
# 				l_price_off_nord=round(float((lowest-avp)/avp),2)
# 				h_price_off_nord=round(float((highest-avp)/avp),2)
#
# 				price_off_nord_range=str(l_price_off_nord)+"-"+str(h_price_off_nord)
# 				#price_off_nord=str(l_price_off_nord)+"-"+str(h_price_off_nord)
#
# 			#calculate the comp_available_price_off_nord, which is not done
#
# 		towrite.append(price_off_nord)
# 		towrite.append(comp_available_price)
# 		towrite.append(price_off_nord_range) #this has to be changed, when we calculate comp_available_price_off_nord
#
# 		towrite.append(comp_sizes)
# 		if comp_sizes!='NA':
# 			count_size=len(comp_sizes.split(','))
# 		else:
# 			count_size='NA'
# 		towrite.append(count_size)
# 		towrite.append(k)
# 		towrite.append(j)
# 		towrite.append(data.get("crawl_date"))
# 		for i in range(0,10):
# 			towrite.append("NA")
# 		csvfile.writerow(towrite)
# 		towrite=[]
#
#
# #this function parses the products api to read the data bundlewise
# def generate():
#
#
# 	start = 0
# 	limit = 2000
#
# 	#count = 1000
#
# 	prev_bid=0
# 	comp_data={}
# 	while True:
#
# 		api_url = "http://app.dataweave.com/v6/app/retailer/bundle_products/?&api_key=fb3662cb688780a591f64047aa0a906a&start="+str(start)+"&limit="+str(limit)+"&base_view=all_products"
# 		print api_url
#
# 		datas = requests.get(api_url).json()
#
# 		for data in datas.get("data",[]):
#
# 			bundle_id=data.get("bundle_id")
# 			all_sizes=data.get("all_sizes")
#
# 			stock=data.get("stock","").lower()
# 			available_price=data.get("available_price")
# 			source=data.get("source")
#
# 			if source=="Amazon-US" or source=="Jcrew-US":
# 				try:
# 					size_list=json.loads(all_sizes) #sizes comes as string from API, loading that to JSON, for amazon & jcrew
# 				except:
# 					size_list='NA'
#
# 				if size_list!='NA':
#
# 					#in jcrew sometimes it comes as list of list
# 					try:
# 						all_sizes=','.join(size_list)
# 					except:
# 						all_sizes=','.join(size_list[0])
# 				else:
# 					all_sizes='NA'
#
# 			url=data.get("url")
# 			mrp=data.get("mrp")
#
#
# 			keys=["brand","category","subcategory","bundle_name","seed_sku","crawl_date"]
#
#
#
# 			if bundle_id not in comp_data:
# 				comp_data[bundle_id]={}
#
# 			for k in keys:
# 				comp_data[bundle_id][k]=data.get(k)
# 			if "source_list" not in comp_data[bundle_id]:
# 				comp_data[bundle_id]["source_list"]=[]
#
# 			if source not in comp_data[bundle_id]["source_list"] and source!='ShopNordstrom-US':
# 				comp_data[bundle_id]["source_list"].append(source)
#
#
#
# 			if source!='ShopNordstrom-US':
#
# 				if source not in comp_data[bundle_id] and available_price!="NA":
# 					comp_data[bundle_id][source]=[available_price]
# 				elif available_price!="NA":
# 					comp_data[bundle_id][source].append(available_price)
#
# 				comp_data[bundle_id][source+"_url"]=url
# 				comp_data[bundle_id][source+"_allsizes"]=all_sizes
# 			else:
# 				comp_data[bundle_id]["seed_sizes"]=all_sizes
# 				comp_data[bundle_id]["available_price"]=available_price
#
#
#
#
#
# 			if prev_bid!=0 and prev_bid!=bundle_id:
# 				writerow(comp_data[prev_bid]) #whenever the bid changes call the function to write the data to csv
# 			prev_bid=bundle_id
#
# 		#if start>100:
# 			#break;
# 		if len(datas.get("data",[])) <= 0:
# 			break;
# 		start+=limit
#
#
#
# if __name__ == '__main__':
#
# 	mapping=static_data() #get the static data
#
# 	filename="shopnordstrom_report.csv"
# 	cfile =open(filename,'w+')
#
# 	csvfile = csv.writer(cfile)
# 	csvfile.writerow(["style_id","style_num","color_code","color_description","nord_size_type_description","sku","brand","category","subcategory","product_name","nord_price_all_sizes","nord_available_sizes","nord_available_sizes_count","competitor","comp_url","comp_match_price","comp_price_off_nord","comp_available_price","comp_available_price_off_nord","comp_available_sizes","comp_available_sizes_count","overlap(yes/no)","validation_required","last_scan_date","nord_perc_of_sales", "nord_sale_flag", "page_views", "hct", "lw_report_flag", "comp_perc_of_sales", "price_effective_dates", "on_promo", "promo_name", "promo_effective_date" ])
#
# 	generate()
import csv
import time
import size_matching_Nike_withSize

t = time.strftime("%Y%m%d")
file_prefix = t + "_"

# input_file = "Style List For Dataweave_1000_19.02.19 with image url.csv"
input_file = "Style_List_For_Dataweave_1000_19.02.19_with_image_url.csv"
report_file = file_prefix + "Latest_nike_report.csv"

# json_file_name = "Amazon-US_1250_13_30-pl_copy_json_generic-Process-5.json"
json_file_name = "final_nike_200.json"

def ReadFromCrawledJson():
	fp_json = open(json_file_name, "r")
	for line in fp_json:
		# print "line",line
		row = json.loads(line)
		# print row
		source = row['source']
		# print "testig"
		if source == 'Amazon-US':
			# if row.get('default_seller', '') == '' or row.get('default_seller').lower().startswith('amazon'):
			if row.get('default_seller','').lower().startswith('amazon'):
				seller_flag = 'Y'
			else:
				seller_flag = 'N'
			crawled_data[row['urlh']] = [row.get('all_sizes'), len(row.get('all_sizes',[])), row.get('default_seller',''), row.get('color'), seller_flag]
			# raw_input(row['urlh'])
			# raw_input(crawled_data[row['urlh']])
		elif source == 'Walmart-US':
			# if row.get('default_seller', '') =='' or row.get('default_seller').lower().startswith('walmart'):
			if row.get('default_seller','').lower().startswith('walmart'):
				seller_flag = 'Y'
			else:
				seller_flag = 'N'
			crawled_data[row['urlh']] = [row.get('all_sizes'), len(row.get('all_sizes',[])), row.get('default_seller',''), row.get('color'), seller_flag]
		elif source == 'Ebay-US':
			crawled_data[row['urlh']] = [row.get('all_sizes'), len(row.get('all_sizes',[])), row.get('default_seller',''), row.get('color'), 'N']
		else:
			crawled_data[row['urlh']] = [row.get('all_sizes'), len(row.get('all_sizes',[])), row.get('default_seller',''), row.get('color'), 'Y']

		#logic for handling different all_sizes in different sources
		list_all_sizes = ["Ebay-US", "NordStromRack-US", "Zappos-US", "Kohls-US", "DicksSportingGoods-US"]
		# print row
		if source in list_all_sizes:
			# all_sizes_count = len(row.get('all_sizes', []))
			# print 'f', row
			crawled_data[row['urlh']][0] = ",".join(row.get('all_sizes',[])).replace("[out of stock]","").replace("(NO LID TO BOX)","")
		elif source == "Amazon-US":
			# try:

			if row.get('all_sizes', []):
				try:
					# print 'data in if -->', row.get('all_sizes', [])
					temp_sizes = json.loads(row.get('all_sizes', []))
				except:
					temp_sizes = json.loads('['+row.get('all_sizes')+']')
			else:
				# raw_input(row.get('all_sizes', []))
				# raw_input(row)
				temp_sizes = []
			crawled_data[row['urlh']][1] = len(temp_sizes)
			crawled_data[row['urlh']][0] = ",".join(temp_sizes).replace("[out of stock]","").replace("(NO LID TO BOX)","")
			# except:
			# 	print row['urlh']
		else:
			# print row['all_sizes']
			# raw_input("source {0} row.get('all_sizes') {1}".format(source,row.get('all_sizes')))
			pass

	# print crawled_data
	# raw_input()
	# return crawled_data


		# if row.get('http_status')=="200":
		# 	if "#!#" in row["url"]:
		# 		url_sku = row["url"].split("#!#")[-1]
		# 	else:
		# 		url_sku=row["sku"]
		# 	dic={}
		# 	dic["available_price"]=row.get('available_price')
		# 	dic["all_sizes"]=row.get('all_sizes')
		# 	crawled_whole_json[url_sku] = dic


def generate():


	start = 0
	limit = 2000

	#count = 1000

	prev_bid=0
	comp_data={}
	scnt = 0
	fcnt = 0
	ncnt = 0
	failed_urlh = []
	sem_failed_urlhs = []

	size_missing_count = 0

	while True:

		# api_url = "http://app.dataweave.com/v6/app/retailer/bundle_products/?&api_key=fb3662cb688780a591f64047aa0a906a&start="+str(start)+"&limit="+str(limit)+"&base_view=all_products"
		api_url = "http://app.dataweave.com/v6/app/retailer/bundle_products/?&api_key=8ce5dd0c507678326fe6d5989bde1c48&start="+str(start)+"&limit="+str(limit)+"&base_view=all_products"
		print api_url

		datas = requests.get(api_url).json()

		# raw_input("data count {0} {1}".format(str(datas.get("data",[]))[:100],len(datas.get("data",[]))))
		for data in datas.get("data",[]):
			# print data.keys()
			# print json.dumps(data)

			if data["source"] == "Nike-US":
				ncnt += 1
				continue
			size_missing_count += 1

			# csvfile.writerow(
			# 	["Nike Style-Color Code"[2], "Product Type"[0], "Nike Description"[5], "Nike Color Description"[6],
			# 	"MSRP"[7], "Scrape Date", "Scrape Time", "List of Sizes available", "Is core size Available?", "Count of Sizes", "All core sizes available?",
			# 	 "Retailer", "1st party seller", "Retailer Type", "Product URL", "URL of Product Image",
			# 	 "Retailer's SKU# ", "Retailer's Prod Description", "Retailer's Color Desc", "Site Promotions",
			# 	 "Current Advertised Price", "Current Price in Cart"])
			crawl_date = data.get("crawl_date")
			crawl_time = data.get("crawl_time")
			seed_sku = data.get("seed_sku")

			#all_sizes = data.get("all_sizes","NA")
			# if all_sizes == "NA":
			# 	size_count = 0
			# else:
			# 	size_count = len(all_sizes)
			retailer = data.get("source",'')
			urlh = data.get("urlh")
			if data.get('source','') == 'Amazon-US' or data.get('source','') == 'Ebay-US':
				seller_first_party = 'N'
			else:
				# seller = data.get('')
				seller_first_party = 'Y'
			# Retailer_type = data.get("all_sizes",[])
			product_url = data.get("url",'').encode("utf-8","ignore")
			product_img_url = data.get("thumbnail",'').encode("utf-8","ignore")
			sku = data.get("sku",'')
			seller = data.get("seller","")
			product_desc = data.get("title",'').encode("utf-8","ignore")
			color = data.get("color",'')
			promotion_text = data.get("promo_code",'')
			mrp = data.get("mrp",'')
			available_price = data.get("available_price",'')

			if urlh in crawled_data:
				scnt += 1
				# row.get('all_sizes'), row.get('default_seller', ''), row.get('color'), seller_flag
				# print "j",crawled_data[urlh]
				if crawled_data[urlh][0]:
					# print "tes",crawled_data[urlh][0]
					print "tes source",data['source']
					print "tes1",data['urlh']
					print "tes2",crawled_data[urlh][0]
					all_sizes = crawled_data[urlh][0].encode("utf-8","ignore")
				else:
					all_sizes = crawled_data[urlh][0]
				size_count = crawled_data[urlh][1]
				seller = crawled_data[urlh][2].encode("utf-8","ignore")
				# color = crawled_data[urlh][3].encode("utf-8","ignore")
				if crawled_data[urlh][3]:
					color = crawled_data[urlh][3].encode("utf-8","ignore")
				else:
					color = crawled_data[urlh][3]
				seller_first_party = crawled_data[urlh][4].encode("utf-8","ignore")
			else:
				fcnt += 1
				failed_urlh.append(urlh.encode("utf-8","ignore"))
				all_sizes = ''
				size_count = 0

			# print mapping[seed_sku][1]
			# print mapping[seed_sku][2]

			# raw_input(size_matching_Nike_withSize.match(mapping[seed_sku][1],mapping[seed_sku][2],all_sizes))

			if all_sizes:
				try:
					print mapping[seed_sku][1],
					print mapping[seed_sku][2],
					print all_sizes
					matchFound,AllCoreSizeFound,countMatchFound,verificationReq = size_matching_Nike_withSize.match(mapping[seed_sku][1],mapping[seed_sku][2],all_sizes)
					# raw_input("Result {0} {1} {2} {3}".format(matchFound,AllCoreSizeFound,countMatchFound,verificationReq))
					# print "crawled data"
					# raw_input(data)
				except:
					sem_failed_urlhs.append([urlh,all_sizes])
					print "error",all_sizes,mapping[seed_sku][1],mapping[seed_sku][2]
					print "error",type(all_sizes)
					print "error",data
					# raw_input(sem_failed_urlhs)

					# raw_input()
					# matchFound, AllCoreSizeFound, countMatchFound, verificationReq = False,True,False,0
			else:
				matchFound, AllCoreSizeFound, countMatchFound, verificationReq = False, False, False, 0
			record = [crawl_date, crawl_time, all_sizes, matchFound, size_count,
					  AllCoreSizeFound, retailer, seller, seller_first_party, "",
					  product_url, product_img_url, sku, product_desc, color, promotion_text, mrp, available_price]

			# mapping[seed_sku].append(all_sizes)
			# mapping[seed_sku].extend(record)
			newlist = mapping.get(seed_sku) + record

			# print "mapping[seed_sku]",mapping[seed_sku]
			# raw_input(mapping[seed_sku])

			csvfile.writerow(newlist)
			# raw_input()
		if len(datas.get("data", [])) <= 0:
			break;
		start += limit
		# raw_input(start)

	print "SCount",scnt
	print "FCount",fcnt
	print "NCount",ncnt
	# print "urlh list",failed_urlh
	print "Failed urlh list",sem_failed_urlhs
	print "len(Failed urlh) list",len(sem_failed_urlhs)
	print "total sources size count",size_missing_count
		# break

			# bundle_id=data.get("bundle_id")
			# all_sizes=data.get("all_sizes")
			#
			# stock=data.get("stock","").lower()
			# available_price=data.get("available_price")
			# source=data.get("source")
			#
			# if source=="Amazon-US" or source=="Jcrew-US":
			# 	try:
			# 		size_list=json.loads(all_sizes) #sizes comes as string from API, loading that to JSON, for amazon & jcrew
			# 	except:
			# 		size_list='NA'
			#
			# 	if size_list!='NA':
			#
			# 		#in jcrew sometimes it comes as list of list
			# 		try:
			# 			all_sizes=','.join(size_list)
			# 		except:
			# 			all_sizes=','.join(size_list[0])
			# 	else:
			# 		all_sizes='NA'
			#
			# url=data.get("url")
			# mrp=data.get("mrp")
			#
			#
			# keys=["brand","category","subcategory","bundle_name","seed_sku","crawl_date"]

def static_data():
	static_mapping={}
	with open(input_file) as bsvfile:
		spamreader = csv.reader(bsvfile, delimiter=',')
		for row in spamreader:
			if 'Category' not in row:
				pid=row[2]
				if pid not in static_mapping:
					# static_mapping[pid]=[row[2],row[0].encode("utf-8", "ignore"),row[5].encode("utf-8", "ignore"),row[6].encode("utf-8", "ignore"),row[7].encode("utf-8", "ignore")]
					static_mapping[pid]=[row[2],row[0].encode("utf-8", "ignore"),row[1].encode("utf-8", "ignore"),row[6].encode("utf-8", "ignore"),row[7].encode("utf-8", "ignore"),row[8].encode("utf-8", "ignore")]

	return static_mapping


if __name__ == '__main__':
	mapping=static_data() #get the static data
	print len(mapping)
	# print mapping.keys()
	# print mapping

	crawled_data = {}
	ReadFromCrawledJson()
	cfile =open(report_file,'w+')
	csvfile = csv.writer(cfile)
	# csvfile.writerow(["Scrape Date", "Scrape Time", "Nike Style-Color Code", "Nike Description", "Nike Color Description", "Product Type", "List of Sizes available", "Is core size Available?", "Count of Sizes", "All core sizes available?", "Retailer", "1st party seller", "Retailer Type", "Product URL", "URL of Product Image", "Retailer's SKU# ", "Retailer's Prod Description", "Retailer's Color Desc", "Site Promotions", "Current Advertised Price", "Current Price in Cart"])
	# csvfile.writerow(["Nike #", "Category", "Gender", "Style Description", "Color", "MSRP", "Scrape Date", "Scrape Time", "Nike Style-Color Code", "Nike Description", "Nike Color Description", "Product Type", "List of Sizes available", "Is core size Available?", "Count of Sizes", "All core sizes available?", "Retailer", "1st party seller", "Retailer Type", "Product URL", "URL of Product Image", "Retailer's SKU# ", "Retailer's Prod Description", "Retailer's Color Desc", "Site Promotions", "Current Advertised Price", "Current Price in Cart"])
	csvfile.writerow(["Nike Style-Color Code", "Product Type", "Gender", "Nike Description", "Nike Color Description",
					  "Nike_MSRP", "Scrape Date", "Scrape Time", "List of Sizes available", "Is core size Available?",
					  "Count of Sizes", "All core sizes available?", "Retailer", "Seller", "1st party seller", "Retailer Type",
					  "Product URL", "URL of Product Image", "Retailer's SKU# ", "Retailer's Prod Description",
					  "Retailer's Color Desc", "Site Promotions", "Current Advertised Price", "Current Price in Cart"])

	generate()