# from size_matching import match
import simplejson as json
import requests
import csv
import sys



# #function to get the static data, mapping based on seed SKU
# def static_data():
# 	static_mapping={}
# 	with open("NS_Report_fields.csv") as bsvfile:
# 		spamreader = csv.reader(bsvfile, delimiter=',', quotechar='"')
# 		for row in spamreader:
# 			if 'SizeDisplayName' not in row:
# 				pid=row[-1]
# 				if pid not in static_mapping:
# 					static_mapping[pid]=[row[4],row[3],row[2],row[1],row[0]]
# 		return static_mapping
#
#
# #function to write the data to csv
# def writerow(data):
#
# 	seed_sizes=data.get("seed_sizes","NA").encode('utf-8','ignore')
# 	category=data.get("category").encode("utf-8","ignore").encode("utf-8","ignore")
# 	subcategory=data.get("subcategory").encode("utf-8","ignore")
# 	brand=data.get("brand").encode("utf-8","ignore")
# 	seed_sku=data.get("seed_sku")
# 	title=data.get("bundle_name").encode("utf-8","ignore")
#
# 	avp=data.get("available_price")
# 	mrp=data.get("mrp")
#
# 	if seed_sizes!='NA':
# 		count_size=len(seed_sizes.split(','))
# 	else:
# 		count_size='NA'
#
#
# 	for s in data.get("source_list"):
#
# 		static=mapping.get(seed_sku)
#
# 		towrite=static+[seed_sku,brand,category,subcategory,title,avp,seed_sizes,count_size]
#
# 		comp_sizes=data.get(s+"_allsizes","NA").encode('utf-8','ignore')
#
#
# 		if comp_sizes!="NA" or seed_sizes!="NA": #only calling the semantics module if there is valid size in either of seed or comp
# 			try:
# 				k,j=match(category,subcategory,seed_sizes,comp_sizes) #calling semantics API for checking the size match
# 			except:
# 				k='NA'
# 				j='NA'
# 		else:
# 			k='NA'
# 			j='NA'
#
# 		towrite.append(s)
# 		towrite.append(data.get(s+"_url","").encode("utf-8","ignore"))
#
# 		if s in data:
# 			price=data.get(s)
# 			if s!='Amazon-US':
# 				price=price[0]
# 				comp_available_price=price #same as price
# 			else:
# 				price.sort()
# 				lowest=price[0]
# 				highest=price[-1]
#
# 				price=lowest
# 				comp_available_price=str(lowest)+"-"+str(highest)
# 		else:
# 			price="NA"
# 			comp_available_price="NA"
#
# 		towrite.append(price)
# 		if avp=="NA" or price=="NA":
# 			price_off_nord="NA"
# 			price_off_nord_range="NA"
# 		else:
# 			print avp
# 			print price
#
# 			price_off_nord=round(float((price-avp)/avp),2)
#
# 			if s!='Amazon-US':
# 				price_off_nord_range=price_off_nord #both are same
# 			else:
# 				l_price_off_nord=round(float((lowest-avp)/avp),2)
# 				h_price_off_nord=round(float((highest-avp)/avp),2)
#
# 				price_off_nord_range=str(l_price_off_nord)+"-"+str(h_price_off_nord)
# 				#price_off_nord=str(l_price_off_nord)+"-"+str(h_price_off_nord)
#
# 			#calculate the comp_available_price_off_nord, which is not done
#
# 		towrite.append(price_off_nord)
# 		towrite.append(comp_available_price)
# 		towrite.append(price_off_nord_range) #this has to be changed, when we calculate comp_available_price_off_nord
#
# 		towrite.append(comp_sizes)
# 		if comp_sizes!='NA':
# 			count_size=len(comp_sizes.split(','))
# 		else:
# 			count_size='NA'
# 		towrite.append(count_size)
# 		towrite.append(k)
# 		towrite.append(j)
# 		towrite.append(data.get("crawl_date"))
# 		for i in range(0,10):
# 			towrite.append("NA")
# 		csvfile.writerow(towrite)
# 		towrite=[]
#
#
# #this function parses the products api to read the data bundlewise
# def generate():
#
#
# 	start = 0
# 	limit = 2000
#
# 	#count = 1000
#
# 	prev_bid=0
# 	comp_data={}
# 	while True:
#
# 		api_url = "http://app.dataweave.com/v6/app/retailer/bundle_products/?&api_key=fb3662cb688780a591f64047aa0a906a&start="+str(start)+"&limit="+str(limit)+"&base_view=all_products"
# 		print api_url
#
# 		datas = requests.get(api_url).json()
#
# 		for data in datas.get("data",[]):
#
# 			bundle_id=data.get("bundle_id")
# 			all_sizes=data.get("all_sizes")
#
# 			stock=data.get("stock","").lower()
# 			available_price=data.get("available_price")
# 			source=data.get("source")
#
# 			if source=="Amazon-US" or source=="Jcrew-US":
# 				try:
# 					size_list=json.loads(all_sizes) #sizes comes as string from API, loading that to JSON, for amazon & jcrew
# 				except:
# 					size_list='NA'
#
# 				if size_list!='NA':
#
# 					#in jcrew sometimes it comes as list of list
# 					try:
# 						all_sizes=','.join(size_list)
# 					except:
# 						all_sizes=','.join(size_list[0])
# 				else:
# 					all_sizes='NA'
#
# 			url=data.get("url")
# 			mrp=data.get("mrp")
#
#
# 			keys=["brand","category","subcategory","bundle_name","seed_sku","crawl_date"]
#
#
#
# 			if bundle_id not in comp_data:
# 				comp_data[bundle_id]={}
#
# 			for k in keys:
# 				comp_data[bundle_id][k]=data.get(k)
# 			if "source_list" not in comp_data[bundle_id]:
# 				comp_data[bundle_id]["source_list"]=[]
#
# 			if source not in comp_data[bundle_id]["source_list"] and source!='ShopNordstrom-US':
# 				comp_data[bundle_id]["source_list"].append(source)
#
#
#
# 			if source!='ShopNordstrom-US':
#
# 				if source not in comp_data[bundle_id] and available_price!="NA":
# 					comp_data[bundle_id][source]=[available_price]
# 				elif available_price!="NA":
# 					comp_data[bundle_id][source].append(available_price)
#
# 				comp_data[bundle_id][source+"_url"]=url
# 				comp_data[bundle_id][source+"_allsizes"]=all_sizes
# 			else:
# 				comp_data[bundle_id]["seed_sizes"]=all_sizes
# 				comp_data[bundle_id]["available_price"]=available_price
#
#
#
#
#
# 			if prev_bid!=0 and prev_bid!=bundle_id:
# 				writerow(comp_data[prev_bid]) #whenever the bid changes call the function to write the data to csv
# 			prev_bid=bundle_id
#
# 		#if start>100:
# 			#break;
# 		if len(datas.get("data",[])) <= 0:
# 			break;
# 		start+=limit
#
#
#
# if __name__ == '__main__':
#
# 	mapping=static_data() #get the static data
#
# 	filename="shopnordstrom_report.csv"
# 	cfile =open(filename,'w+')
#
# 	csvfile = csv.writer(cfile)
# 	csvfile.writerow(["style_id","style_num","color_code","color_description","nord_size_type_description","sku","brand","category","subcategory","product_name","nord_price_all_sizes","nord_available_sizes","nord_available_sizes_count","competitor","comp_url","comp_match_price","comp_price_off_nord","comp_available_price","comp_available_price_off_nord","comp_available_sizes","comp_available_sizes_count","overlap(yes/no)","validation_required","last_scan_date","nord_perc_of_sales", "nord_sale_flag", "page_views", "hct", "lw_report_flag", "comp_perc_of_sales", "price_effective_dates", "on_promo", "promo_name", "promo_effective_date" ])
#
# 	generate()
import csv
import time
import size_matching_Nike_withSize

t = time.strftime("%Y%m%d")
file_prefix = t + "_"

# input_file = "Style List For Dataweave_1000_19.02.19 with image url.csv"
input_file = "Style_List_For_Dataweave_1000_19.02.19_with_image_url.csv"
report_file = file_prefix + "Latest_nike_report.csv"

# input_file = "demo.csv"
# input_file = "Nike_Custom_Report - 20190307_Latest_nike_report.csv"
input_file = "custom_nike_latest.csv"
def static_data():
	static_mapping={}
	report_file = file_prefix+'Final_Custom_Report.csv'
	with open(input_file) as bsvfile:
		spamreader = csv.reader(bsvfile, delimiter=',')
		for row in spamreader:
			if 'Nike Style-Color Code' not in row:
				pid=row[2]
				cat=row[1].encode("utf-8","ignore")
				sc=row[2].encode("utf-8","ignore")
				try:
					all_size=row[8].encode("utf-8","ignore")
				except:
					print "error",row[10]
				if pid not in static_mapping:
					print cat,sc,all_size

				if all_size.startswith('['):
					all_size = all_size[1:]
				if all_size.endswith(']'):
					all_size = all_size[:-1]
				all_size = all_size.replace("[out of stock]","").replace("(NO LID TO BOX)","")

				print "size",all_size

				try:
					matchFound, AllCoreSizeFound, countMatchFound, verificationReq = size_matching_Nike_withSize.match(cat, sc, all_size)
				except:
					print "semantics error"

				row[8] = all_size
				row[10] = matchFound
				row[11] = len(all_size.split(','))
				row[12] = AllCoreSizeFound
				print matchFound,AllCoreSizeFound
				# raw_input(row)
				csvfile.writerow(row)
			else:
				cfile = open(report_file, 'w+')
				csvfile = csv.writer(cfile)
				# csvfile.writerow(["Scrape Date", "Scrape Time", "Nike Style-Color Code", "Nike Description", "Nike Color Description", "Product Type", "List of Sizes available", "Is core size Available?", "Count of Sizes", "All core sizes available?", "Retailer", "1st party seller", "Retailer Type", "Product URL", "URL of Product Image", "Retailer's SKU# ", "Retailer's Prod Description", "Retailer's Color Desc", "Site Promotions", "Current Advertised Price", "Current Price in Cart"])
				# csvfile.writerow(["Nike #", "Category", "Gender", "Style Description", "Color", "MSRP", "Scrape Date", "Scrape Time", "Nike Style-Color Code", "Nike Description", "Nike Color Description", "Product Type", "List of Sizes available", "Is core size Available?", "Count of Sizes", "All core sizes available?", "Retailer", "1st party seller", "Retailer Type", "Product URL", "URL of Product Image", "Retailer's SKU# ", "Retailer's Prod Description", "Retailer's Color Desc", "Site Promotions", "Current Advertised Price", "Current Price in Cart"])
				csvfile.writerow(row)

				# generate()

			# static_mapping[pid]=[row[2],row[0].encode("utf-8", "ignore"),row[5].encode("utf-8", "ignore"),row[6].encode("utf-8", "ignore"),row[7].encode("utf-8", "ignore")]
					# static_mapping[pid]=[row[2],row[0].encode("utf-8", "ignore"),row[1].encode("utf-8", "ignore"),row[6].encode("utf-8", "ignore"),row[7].encode("utf-8", "ignore"),row[8].encode("utf-8", "ignore")]

	return static_mapping


if __name__ == '__main__':
	mapping=static_data() #get the static data
	print len(mapping)
	print mapping.keys()
	print mapping
	#
	# crawled_data = {}
	# ReadFromCrawledJson()
	# cfile =open(report_file,'w+')
	# csvfile = csv.writer(cfile)
	# # csvfile.writerow(["Scrape Date", "Scrape Time", "Nike Style-Color Code", "Nike Description", "Nike Color Description", "Product Type", "List of Sizes available", "Is core size Available?", "Count of Sizes", "All core sizes available?", "Retailer", "1st party seller", "Retailer Type", "Product URL", "URL of Product Image", "Retailer's SKU# ", "Retailer's Prod Description", "Retailer's Color Desc", "Site Promotions", "Current Advertised Price", "Current Price in Cart"])
	# # csvfile.writerow(["Nike #", "Category", "Gender", "Style Description", "Color", "MSRP", "Scrape Date", "Scrape Time", "Nike Style-Color Code", "Nike Description", "Nike Color Description", "Product Type", "List of Sizes available", "Is core size Available?", "Count of Sizes", "All core sizes available?", "Retailer", "1st party seller", "Retailer Type", "Product URL", "URL of Product Image", "Retailer's SKU# ", "Retailer's Prod Description", "Retailer's Color Desc", "Site Promotions", "Current Advertised Price", "Current Price in Cart"])
	# csvfile.writerow(["Nike Style-Color Code", "Product Type", "Gender", "Nike Description", "Nike Color Description",
	# 				  "Nike_MSRP", "Scrape Date", "Scrape Time", "List of Sizes available", "Is core size Available?",
	# 				  "Count of Sizes", "All core sizes available?", "Retailer", "Seller", "1st party seller", "Retailer Type",
	# 				  "Product URL", "URL of Product Image", "Retailer's SKU# ", "Retailer's Prod Description",
	# 				  "Retailer's Color Desc", "Site Promotions", "Current Advertised Price", "Current Price in Cart"])
	#
	# generate()