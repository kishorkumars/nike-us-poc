import valueExtractionWhole as ve
import xlrd
import xlwt
workbook = xlwt.Workbook(encoding = 'ascii')
worksheet = workbook.add_sheet('SizeMatch')


# Give the location of the file 
loc = ("nords_sample_file_size.xlsx") 
  
# To open Workbook 
wb = xlrd.open_workbook(loc) 
sheet = wb.sheet_by_index(0)
nrows=sheet.nrows
nrows=6400
ncols=sheet.ncols
# ncols=

def removeDecimal(rawString):
	if ".0 " in " "+rawString+" ":
		return rawString.replace(".0","")
	else:
		return rawString


def removeAscii(rawString):
	return ''.join([i if ord(i) < 128 else ' ' for i in rawString])

def preprocessNorm(sizeList):
	normSizeList=[]
	for i in sizeList:
		normSizeList.append(''.join(e for e in i if e.isalnum()).lower())
	return normSizeList

def xstr(s):
	'''Return '' if string is None'''
	return '' if s is None else str(s)

def mergeDict(dict1,dict2):
	mergedict=dict1
	for index,newdict in enumerate(mergedict):
		mergedict[index]['alpha'].extend(dict2[index]['alpha'])
		mergedict[index]['alpha']=list(set(mergedict[index]['alpha']))
	return mergedict

def match(category="default",subcategory="default",nordstromAvailSizeText="",compAvailSizeText=""):
	'''
	Input: category,subcategory: should be as per Normalizated/ folder
		   nordstromAvailSizeText: comma seperated list of size in String format
		   compAvailSizeText: comma seperated list of size in String format
	Output: matchFound: True/False, if there is overlapping size present
			verificationReq: True/False, If QA verification is needed.
	'''
	#To get merged Prdt type
	prdtType=(category.lower()+subcategory.lower()).replace(" ","").replace("/","")

	#To handle empty Size List String- Taking as One Size
	if str(nordstromAvailSizeText)=="":
		nordstromAvailSize=['One Size']
	else:
		nordstromAvailSize=removeAscii(str(nordstromAvailSizeText)).split(",")
		nordstromAvailSize=[removeDecimal(x) for x in nordstromAvailSize]
	if str(compAvailSizeText)=="":
		compAvailSize=['One Size']
	else:
		compAvailSize=removeAscii(str(compAvailSizeText)).split(",")	
		compAvailSize=[removeDecimal(x) for x in compAvailSize]

	matchFound,verificationReq=False,True

	#Rule 1: Straight Match:-If simple preprocessing gets a match.
	Norm_nordstromAvailSize=preprocessNorm(nordstromAvailSize)
	Norm_compAvailSize=preprocessNorm(compAvailSize)
	if any(x in Norm_compAvailSize for x in Norm_nordstromAvailSize):
		matchFound=True
		verificationReq=False
		return matchFound,verificationReq

	for size in nordstromAvailSize:
		if any(" "+x.lower()+" " in " "+size.lower()+" " for x in compAvailSize):
			matchFound,verificationReq=True,False
			return matchFound,verificationReq
	for size in compAvailSize:
		if any(" "+x.lower()+" " in " "+size.lower()+" " for x in nordstromAvailSize):
			matchFound,verificationReq=True,False
			return matchFound,verificationReq

	#Rule2: From normalization Module
	if matchFound==False and verificationReq==True:
		Norm_nordstromAvailSize=[]
		def_Norm_nordstromAvailSize=[]
		def_Norm_compAvailSize=[]
		Norm_compAvailSize=[]
		Alpha_nord=[]
		Alpha_comp=[]
		SizeUnit_nord=[]
		SizeUnit_comp=[]
		size_nord=[]
		size_comp=[]
		unit_comp=False
		unit_nord=False
		for eachSize in nordstromAvailSize:
			Norm_nordstromAvailSize.append(ve.normalize(eachSize,prdtType))
			def_Norm_nordstromAvailSize.append(ve.normalize(eachSize,"defaultdefault"))
			Norm_nordstromAvailSize=mergeDict(Norm_nordstromAvailSize,def_Norm_nordstromAvailSize)
			Alpha_nord.extend(Norm_nordstromAvailSize[-1]['alpha'])
			for num,i in enumerate(Norm_nordstromAvailSize[-1]['numeric']['number']):
				size_nord.append(i)
				SizeUnit_nord.append(i+xstr(Norm_nordstromAvailSize[-1]['numeric']['unit'][num]))
				if Norm_nordstromAvailSize[-1]['numeric']['unit'][num] is not None:
					unit_nord=True
		for eachSize in compAvailSize:
			Norm_compAvailSize.append(ve.normalize(eachSize,prdtType))
			def_Norm_compAvailSize.append(ve.normalize(eachSize,"defaultdefault"))
			Norm_compAvailSize=mergeDict(Norm_compAvailSize,def_Norm_compAvailSize)
			Alpha_comp.extend(Norm_compAvailSize[-1]['alpha'])
			for num,i in enumerate(Norm_compAvailSize[-1]['numeric']['number']):
				size_comp.append(i)
				SizeUnit_comp.append(i+xstr(Norm_compAvailSize[-1]['numeric']['unit'][num]))
				if Norm_compAvailSize[-1]['numeric']['unit'][num] is not None:
					unit_comp=True
		print Norm_nordstromAvailSize
		print Norm_compAvailSize

		#Rule2: Straight match from alpha size
		if '' in Alpha_nord:
			Alpha_nord=list(set(Alpha_nord)).remove('')
		if '' in Alpha_comp:
			Alpha_comp=list(set(Alpha_comp)).remove('')
		if len(set(Alpha_nord).intersection(Alpha_comp))>0:
			matchFound,verificationReq=True,False
			return matchFound,verificationReq

		#Rule3: NumberUnit concat match
		if '' in SizeUnit_nord:
			SizeUnit_nord=list(set(SizeUnit_nord)).remove('')
		if '' in SizeUnit_comp:
			SizeUnit_comp=list(set(SizeUnit_comp)).remove('')
		try:
			if len(set(SizeUnit_nord).intersection(set(SizeUnit_comp)))>0:
				matchFound,verificationReq=True,False
				return matchFound,verificationReq
		except:
			matchFound,verificationReq=False,True
			return matchFound,verificationReq


		
		#Rule4: Only numbers match, Incase unit is missing in one 
		if (unit_comp ^ unit_nord):

			if '' in size_nord:
				size_nord=list(set(size_nord)).remove('')
			if '' in size_comp:
				size_comp=list(set(size_comp)).remove('')
			if len(set(size_nord).intersection(set(size_comp)))>0:
				matchFound,verificationReq=True,False
				return matchFound,verificationReq
		
		#Rule5: Only numbers match, when everything else fails, but will require verification from QA. 

		if '' in size_nord:
			size_nord=list(set(size_nord)).remove('')
		if '' in size_comp:
			size_comp=list(set(size_comp)).remove('')
		if len(set(size_nord).intersection(set(size_comp)))>0:
			matchFound,verificationReq=True,True
			return matchFound,verificationReq

	return matchFound,verificationReq

def main():	
	for row in range(1,nrows):
		try:
			matchFound=False
			verificationReq=True
			srcAvailSize=sheet.cell_value(row,2)
			compAvailSize=sheet.cell_value(row,4)
			matchFound,verificationReq=match("dresses","dress",srcAvailSize,compAvailSize)
		except Exception, e:
			print "Skipped: ",row," " ,e	
		
		worksheet.write(row, 0, sheet.cell_value(row,0))
		worksheet.write(row, 1, sheet.cell_value(row,1))
		worksheet.write(row, 2, sheet.cell_value(row,2))
		worksheet.write(row, 3, sheet.cell_value(row,3))
		worksheet.write(row, 4, sheet.cell_value(row,4))
		worksheet.write(row, 5, sheet.cell_value(row,5))
		worksheet.write(row, 6, str(matchFound))
		worksheet.write(row, 7, str(verificationReq))

	workbook.save('Excel_SizeMatch.xls')


if __name__ == '__main__':
	main()