import csv
import re

def fetchData():
	'''To get data for normalization. Will change frequently'''
	with open('Size_variant_distribution.csv') as csvfile:
		readCSV = csv.reader(csvfile, delimiter=',')
		sizePrdtDict={}
		for rownum,row in enumerate(readCSV):
				if rownum==0:
					continue
				if rownum>20:
					src=row[2].lower().strip(" ").strip().strip('[')
					src=src.strip(']')
					src=src.strip("'")
					comp=row[6].lower().strip(" ").strip().strip('[')
					comp=comp.strip('[')
					comp=comp.strip("'")
					sizePrdtDict[row[4]]={'src':src,'comp':comp,'rownum':str(rownum)}
					# return src,comp
				else:
					continue
	return sizePrdtDict

def removeExtraSpace(data):
	'''Removes extra space from data'''
	return ' '.join(data.split())

def splitText(data,pattern=['()','/',' x ']):
	'''To split data into different candidate text'''
	'''Pattern precedence should be taken care of'''
	'''Take care that regex pattern are handled seperately. and we are extracting multiple candidate in iterative way'''
	retDataList=[]
	for i in pattern:
		if i=='()':
			retDatal=re.findall('\(.*?\)',data)
			retDatal=[x.strip('(').strip(')') for x in retDatal]
			retDataList.extend(retDatal)
			data = re.sub(r'\(.*?\)', '', data)
		else:
			retData=data.split(i)
			retDataList.extend(retData)
	newRetDataList=[]
	for itr in retDataList:
		newRetDataList.append(removeExtraSpace(itr))
	return newRetDataList

def hasNumbers(data):
	'''Returns true if data contains any digit'''
	return any(char.isdigit() for char in data)

def stopwordRemoval(data,stopword_file):
	'''Removes stopwords or phrases specified in stopword_file from data'''
	stopwordList=[]
	actualData=[]
	with open(stopword_file) as fin:
		stopwordList=fin.read().lower().splitlines()
	for dataItr in data:
		for stopwordItr in stopwordList:
			dataItr=dataItr.lower().replace(stopwordItr.lower(),'')
		actualData.append(removeExtraSpace(dataItr))
	return actualData

def extractNumbers(data):
	extracted=''.join([c for c in data if c in '1234567890.-']).strip()
	return re.split('-| ',extracted)

def getNormalizationListFromFile(normalizationFile):
	List=[]
	with open(normalizationFile,'r') as fin:
		LineList=fin.read().lower().splitlines()
		for Line in LineList:
			List.append(Line.split(","))
	return List

def normalize(rawData,PrdtType):
	'''Logic for Normalization
	Input: String, Output: List of String
	1. Split text on pattern into candidates, in case more than one.
	2. Apply stopword removal- remove frequently occuring phrase or words using stopword_file
	3. Get values List'''
	rawData=rawData.lower()
	actualData=splitText(rawData)
	actualData=stopwordRemoval(actualData,stopword_file="stopword_removal")

	'''Creation of normdict'''
	'''Read gender_ageGroup'''
	gender_ageGroupList=getNormalizationListFromFile('gender_ageGroup')
	size_standardList=getNormalizationListFromFile('size_standard')
	unit_List=getNormalizationListFromFile('stopwords_size_unit')
	alphaText_List=getNormalizationListFromFile("Normalized/"+PrdtType)
	normdict={'genderOrAgeGroup':None,'numeric':{'number':[],'unit':[],'size_standard':[]},'alpha':[]}
	for eachDataPoint in actualData:
		'''Handling Gender and Age Group'''
		for i in gender_ageGroupList:
			if any(s in eachDataPoint for s in i):
				normdict['genderOrAgeGroup']=i[0]
				break

		'''Handling Normalizated Alpha'''
		for i in alphaText_List:
			# print i
			if any(" "+s+" " in " "+eachDataPoint.lower()+" " or bool(re.search(r'\d'+s+" ", " "+eachDataPoint.lower()+" ")) for s in i):
				normdict['alpha'].append(i[0].lower())


		'''Handling digits'''
		extractedNumbers=[]
		extractedNumbers=extractNumbers(eachDataPoint)
		normdict['numeric']['number'].extend(extractedNumbers)

		'''Handling size_standards'''
		extractedSizeStandards=[]
		for i in size_standardList:
			if any(s in eachDataPoint for s in i):
				extractedSizeStandards.append(i[0])
		if len(extractedSizeStandards)==0:
			normdict['numeric']['size_standard'].extend([None]*len(extractedNumbers))
		elif len(extractedNumbers)>len(extractedSizeStandards) and len(extractedSizeStandards)==1:
			normdict['numeric']['size_standard'].extend(extractedSizeStandards[0]*len(extractedNumbers))
		else:
			normdict['numeric']['size_standard'].extend(extractedSizeStandards)

		'''Handling units'''
		extractedUnits=[]
		for i in unit_List:
			if any(" "+s+" " in " "+eachDataPoint.lower()+" " or bool(re.search(r'\d'+s+" ", " "+eachDataPoint.lower()+" ")) or bool(re.search(r'\d'+s+"-", " "+eachDataPoint.lower()+" ")) for s in i):
				extractedUnits.append(i[0])
		if len(extractedUnits)==0:
			normdict['numeric']['unit'].extend([None]*len(extractedNumbers))
		elif len(extractedNumbers)>len(extractedUnits) and len(extractedUnits)==1:
			normdict['numeric']['unit'].extend([extractedUnits[0]]*len(extractedNumbers))
		else:
			normdict['numeric']['unit'].extend(extractedUnits)


		
	return normdict


def main():
	'''Working Logic'''
	# dictPrdt=fetchData()
	# for Prdt in dictPrdt:
	# 	src,comp=dictPrdt[Prdt]['src'],dictPrdt[Prdt]['comp']
	# 	PrdtType=Prdt.replace('/','')
	# 	PrdtNum=dictPrdt[Prdt]['rownum']
	# 	fwrite=open('TextForNormalization/'+PrdtNum+'_onlyText_'+PrdtType+'_src','w')
	# 	new=[]
	# 	for rawData in src.split(','):	
	# 		'''Simple preprocessing-is not generalized. Just call normalize function on the text'''
	# 		rawData=rawData.strip().strip("'").strip('"')
	# 		norm=normalize(rawData)
	# 		print "Before: ",rawData,"  After: ",norm
	# 		for itr in norm:
	# 			if not any(char.isdigit() for char in itr):
	# 				new.append(itr)
	# 	new=[i for i in new if i != '']
	# 	for itr in set(new):
	# 		fwrite.write(itr)
	# 		fwrite.write("\n")
	# 	fwrite.close()

	# 	fwrite=open('TextForNormalization/'+PrdtNum+'_onlyText_'+PrdtType+'_comp','w')
	# 	new=[]
	# 	for rawData in comp.split(','):
	# 		'''Simple preprocessing-is not generalized. Just call normalize function on the text'''
	# 		rawData=rawData.strip().strip("'").strip('"')
	# 		norm=normalize(rawData)
	# 		print "Before: ",rawData,"  After: ",norm
	# 		for itr in norm:
	# 			if not any(char.isdigit() for char in itr):
	# 				new.append(itr)
	# 	new=[i for i in new if i != '']
	# 	for itr in set(new):
	# 		fwrite.write(itr)
	# 		fwrite.write("\n")
	# 	fwrite.close()
	
	'''
	srcTextList=['4 US (fits like 0-2)', '9/10', '20 (similar to 20W-22W)', '0 US / 34 IT', 'M (8-10)', '11/12', 'Call 1-888-300-1295', '18 (similar to 16W)', '12 US / 46 IT', 'Petite P', '18-24M', '2-3T', '2-3Y', '10 US / 14 UK', '12-18M', '18 (similar to 14W)', '20', 'Petite', '6 (fits like 8)', '12M', 'XX-Large', '12P', '0', '6 US / 44 IT', '26W', '4', '14', '12X', '5', '6-12M', '2T', 'Small/Medium', '0 P', '2P', '2R', '15-16Y', 'Large P', '2X', '14 US / 50 IT', 'Large', '10R', '7-8', '3/4', '8 US / 42 IT', '28W', '3X-Large', '4T US / 4 AUS', '6 US / 6 AUS', 'XXS (4)', '4 X', '16 US / 52 IT', 'Small P', '2 US / 36 IT', '3-6M', '4 (12-14 US)', '8 US / 12 AU', '4 US / 36 FR', '7-8Y', '2 US / 6 UK', '8 US / 40 FR', '10 US / 44 IT', '3Y', '3X', '0-3M', '10 US / 42 FR', '20 (similar to 18W-20W)', '18 (fits like 20W)', 'L (12-18m)', '12/14', '3T', 'X-Small P', '12Y', '3M', '9-10Y', 'M (6-7)', 'X-Small', '6 US / 38 BR', '12 (fits like 14-16)', '3 X', '2 US / 40 IT', 'Medium', '13/14', '6 US / 40 FR', '24M', 'X-Large', '16 US / 50 IT', '18 US / 54 IT', '16 (fits like 18W)', 'S (3-6m)', '3', '10Y', '4 US / 36 EU', 'Medium P', '7', '6-7 US / 6-7x AUS', '10 US / 14 AU', 'S (7-8)', '10P', '10 US (fits like 10-12)', '0R', '14 US / 46 FR', '0P', 'L (8-10)', '6 US / 10 UK', '11-12', 'M (6-12m)', '0X', '6 US / 38 EU', '4 US / 38 IT', '2 US / 34 FR', '0 US / 38 IT', '2 X', '6-7', '9-10', 'S (14W-16W)', '0P US (fits like 00P)', '2-3 T', '1 (4-6 US)', '10X', '14 US / 48 IT', '3 (10-12 US)', '2T US / 3 AUS', '4P US (fits like 0-2P)', '2 US / 38 IT', '8X', '8Y', '10 US / 42 BR', '16 (similar to 14W-16W)', '24W', '8R', '8P', '12 US / 46 FR', '20 (similar to 14W-16W)', '8 US / 42 FR', '4 US / 40 IT', '8 US / 40 EU', '10 US / 42 EU', '12 US (fits like 14)', '1X', 'M (5-6)', '2 US / 34 EU', '8 US (fits like 6-8)', '8 US / 12 UK', '16/18', '9-12M', '16 (similar to 14W)', '12 US / 16 UK', '8 (fits like 10)', '9M', '12 US / 44 FR', '6 US / 40 IT', 'X-Large P', 'X-Small/Small', '20 (similar to 18W)', 'XS (0-3m)', '2', '6-9M', '6', '22W', '18W (similar to 16W)', '16W', '16P', 'L (13-14y)', '16Y', '5 US / 5 AUS', 'IN STORE ONLY', '3T US / 3 AUS', '1/2', 'XS (6-6x)', '4-5', '12W', '12 US / 48 IT', '18M', '6 US / 42 IT', '8 US / 44 IT', '6 US (fits like 2-4)', '18W', '4 US / 42 IT', '8', 'Small', 'Medium/Large', '0 US / 32 FR', '7/8', '11', '10', '13', '12', '15', '5/6', '17', '16', 'S (3-4T)', '18', '10 US / 44 FR', '6M', '6/6X', '6P', '2 US (fits like 0)', '8 US / 40 BR', '4 US / 8 UK', '2 US / 6 AU', '6X', '6Y', '10 US / 46 IT', 'XS (2-3T)', '18 (similar to 18W-20W)', 'XL (16)', '0 US / 4 UK', '12 US / 44 BR', 'L (14)', '20 (similar to 16W-18W)', '4-5Y / 104-110 cm', '6 US / 38 FR', 'L (12-14)', '2 US / 36 FR', '2P US (fits like 0P)', '14W', '4 US / 36 BR', '20W', 'M (10-12)', 'XX-Small P', 'M (11-12y)', '10-12', '14Y', '1', '00P', 'L (6-7)', 'XS (5-6)', '9', '0-6M', '15/16', '12R', '2 (6-8 US)', '4X', '4Y', '4 US / 8 AU', '4T', 'XL (18-24m)', '4R', '0 US / 36 IT', '4P', '2Y', '8 US / 46 IT', '2-3Y / 92-98 cm', '20/22', 'XX-Small', 'S (9-10y)', '2 US / 34 BR', '6x-7Y / 116-122 cm', '4-5T', '5-6', '14P', '6 US / 10 AU', '00 US / 36IT', '4 US / 38 FR', '0 US / 34 FR', '6-7Y', '3-4Y', '18 (similar to 16W-18W)', '14 US (fits like 16-18)', '5-6Y', '5 X', '18 (similar to 18W)','45 woman']
	compTextList=['2x plus', '9/10', 'lg (us 8-10)', 'md (us 8)', '14p', '11/12', '9months', '12m', 'petite x-small', '12p', '1x (14w-16w)', "sm (women's 4-6)", '46(12)', '12w', '40(8)', '9m', '0x plus', '24', 'lg', '8 uk/4 us', '6 / 7', '20', 'lg (8-10 big kids)', '22', '34 it (00 us)', '18-24m', '10 (big kids)', '12 petite', '2t (toddler)', '29', '24 months', 'sm (us 4-6)', '42(10)', '0', '18months', '5 little kids', '26', '4', 'xx small', '8', 'sm (4-5 little kids)', '24w/xxl', '36(4)', '14 au/xl', '1 / us 4', 'md (us 8-10)', '3m', '10', 'xs (us 0-1)', 'x-large plus', 'xl (us 16)', 'p/s', '3/l', '10 au/m', '0/xs', 'p/l', '38(4)', '40 it (4 us)', '3/4', '3-6m', 'xl (12-14 big kids)', 'lg (12-18 mos)', '14', '2t', '2x(18-20)', 'xs (5/6 little kids)', '48(14)', '0 / us 2', 'petite small', '2x', 'l', '2x (18w-20w)', 'large', 'p', "xxs (women's 00-0)", 'md (6-7 little kids/big kids)', '8 years', '44(10)', 'small', '4 us little kid', '14w/xs', '1x(14-16)', '18-24 months', '14 uk/10 us', '34 fr/0 us', '2/m', 'small(4-5)', '24m', '30', '7-8', 'xs (us 0-2)', '7 big kids', '24w', '24 plus', '3x plus', '14 (big kids)', 'md (us 7-9)', '12y', '12 uk/8 us', 'sm (7/8 big kids)', '10p', '8 big kids', '10 uk/6 us', '38(6)', '2 (us 6)', '20 plus', '00', 'please select', '13/14', '3/6 months', 'sm (us 3-5)', '6 au/xs', 'sm (us 4)', "xl (women's 16)", '3t', '27', '3', 'medium(6-7)', '2 / us 6', '7', '3t toddler', '0 (us 2)', '12 months', '3(medium)', '3-6 months', '12 big kids', "lg (women's 12-14)", 'lg (12/14 big kids)', '4 petite', '4 uk/0 us', '18w/m', 'p/xs', '36 fr/2-4 us', 'xs (us 0)', "md (women's 8-10)", '8p', '42(6)', 'lg (us 12-14)', '48(12)', '36m', '0 petite', "xs (women's 0-2)", '6 uk/2 us', 'xs (5-6 little kids)', '6-12 months', '4p', '0x', 'xl (us 10-12)', '36', 'xxs (4/5 little kids)', '34', '46(14)', 's', '000', '44(8)', '14 petite', '1 (us 4)', '1x', 'medium plus', '8 au/s', '6 (little kids)', '42(8)', 'x large', 'xl (us 13)', '1/s', '40 fr/8 us', 'xl', '2x (us 18w-20w)', '28', '3x(22-24)', '3x (us 20w)', '6months', '34(2)', '6-9m', '36 it (0 us)', '22w', 'xs', '5 (us 12)', '16w', 'extra extra small', 'x-small', '24months', '18/24 mo', 'ps', 'pp', '10 big kids', '2t toddler', 'large(8-10)', '12-18 months', '12/18 mo', '2(small)', 'x-large', '18 plus', '42', '48 it (12 us)', '3 / us 8', '36(0)', '2', '6', '9-12m', 'xx-small', '22 plus', '40(4)', '18 months', 'pl', 'pm', 'xxs', '3/6 mo', 'medium', '1/2', '7/8 (big kids)', 'x-large(12-14)', '4/xl', '3 (us 8)', '6m', '6p', '38 fr/4-6 us', '38', '42 it (6 us)', '6x', 'lg (us 10-12)', 'xxl', '11', 'md', '13', '12', '15', '5/6', '16', '18', '1x plus', '18m', '8 (big kids)', '0-3 months', '18w', '16p', "lg (women's 12)", '4x plus', '12months', '31', '1(x-small)', '14w', '4 little kids', '16w/s', '20w', '1x (us 16w)', '6 little kids', '20w-22w/l', 'xx-large', '2 petite', '12-18m', '6 petite', 'md (10-12 big kids)', '46 it (10 us)', '0x (us 14w)', 'petite large', 'petite medium', '44(12)', '34(0)', '5 (little kids)', '36(2)', '16 petite', '44', 'xs (p)', 'lg (us 10)', '40', '1', '4 / us 10', 'p/m', '5', 'xs (0-3 mos)', '9', '40(6)', '1x (us 14w-16w)', 'sm (us 2-4)', '14 big kids', '15/16', 'lg (us 11)', 'petite', 'xl (us 12-14)', '12 (big kids)', 'x small', 'large plus', 'xx large', 'xs (2t-3t toddler)', '3x', '46(10)', '38(2)', '4 (us 10)', '3 years', 'xs (us 2)', 'md (us 6-8)', 'md (6-12 mos)', 'md (10/12 big kids)', '50 it (14 us)', '6-9 months', '7/8', '0-3m', '10 petite', '4t', '38 it (2 us)', 'm', '22w-24w/xl', '52 it (16 us)', '8 petite', '12 au/l', '6/12 mo', 'sm', 'x-small(2-3)', '2 years', '44 it (8 us)', '0x (12w)', '24w-26w/xxl']
	print len(srcTextList)
	for rawText in srcTextList:
		norm=normalize(rawText,"dressesdress")
		print "Before: ",rawText," After: ",norm
	
	norm=normalize("6x-7Y / 116-122 cm","dressesdress")
	print "Before: ","6x-7Y / 116-122 cm"," After: ",norm
	'''

if __name__ == '__main__':
	main()
