import valueExtractionWhole as ve
import xlrd
import xlwt
import json
import re
# workbook = xlwt.Workbook(encoding = 'ascii')
# worksheet = workbook.add_sheet('SizeMatch')


# Give the location of the file 
loc = ("nords_sample_file_size.xlsx") 
  
# To open Workbook 
# wb = xlrd.open_workbook(loc) 
# sheet = wb.sheet_by_index(0)
# nrows=sheet.nrows
# nrows=6400
# ncols=sheet.ncols
# ncols=

def removeDecimal(rawString):
	if ".0 " in " "+rawString+" ":
		return rawString.replace(".0","")
	else:
		return rawString


def removeAscii(rawString):
	rawString=rawString.encode('utf-8').strip()
	return ''.join([i if ord(i) < 128 else ' ' for i in rawString])

def preprocessNorm(sizeList):
	normSizeList=[]
	for i in sizeList:
		normSizeList.append(re.sub('[^a-zA-Z0-9.]+', ' ', i).lower())
		# normSizeList.append(' '.join(e for e in i if e.isalnum()).lower())
	return normSizeList

def xstr(s):
	'''Return '' if string is None'''
	return '' if s is None else str(s)

def mergeDict(dict1,dict2):
	mergedict=dict1
	for index,newdict in enumerate(mergedict):
		mergedict[index]['alpha'].extend(dict2[index]['alpha'])
		mergedict[index]['alpha']=list(set(mergedict[index]['alpha']))
	return mergedict

def extractNumbers(data):
	extracted=''.join([c for c in data if c in '1234567890.-']).strip()
	return re.split('-| ',extracted)

def match(category="default",subcategory="default",srcAvailSizeText="",compAvailSizeText=""):
	'''
	Input: category,subcategory: should be as per Normalizated/ folder
		   srcAvailSizeText: comma seperated list of size in String format
		   compAvailSizeText: comma seperated list of size in String format
	Output: matchFound: True/False, if there is overlapping size present
			verificationReq: True/False, If QA verification is needed.
	'''
	subcategory=subcategory.lower().strip('s')
	if category.lower()=='footwear':
		if subcategory.lower()=='women' or subcategory.lower()=='woman':
			srcAvailSize=['6','6.5','7','7.5','8','8.5','9','9.5']
		elif subcategory.lower()=='men' or subcategory.lower()=='man':
			srcAvailSize=['9','9.5','10','10.5','11','11.5']

	if category.lower()=='apparel':
		srcAvailSize=['s','m','l']

	compAvailSize=(removeAscii((compAvailSizeText))).replace(" or ",",").replace("-",",").replace("|",",").split(",")	
	compAvailSize=[removeDecimal(x) for x in compAvailSize]

	matchFound,verificationReq,AllCoreSizeFound,countMatchFound=False,True,False,0
	CoreSizeFound=[]

	'''For Footwear Category we do Straight Match'''
	if category.lower()=='footwear':
		CoreSizeFound=[]
		#Rule 1: Straight Match:-If simple preprocessing gets a match.
		Norm_srcAvailSize=preprocessNorm(srcAvailSize)
		Norm_compAvailSize=preprocessNorm(compAvailSize)
		for size in srcAvailSize:
			new_size=extractNumbers(size)
			if any(" "+x.lower()+" " in " "+size.lower()+" " for x in compAvailSize):
				matchFound,verificationReq=True,False
				CoreSizeFound.append(size)
		CoreSizeFound=list(set(CoreSizeFound))
		if len(set(CoreSizeFound))==len(srcAvailSize):
			AllCoreSizeFound=True
		countMatchFound=len(CoreSizeFound)
		print "src:",srcAvailSize,"comp:",compAvailSize,"found:",CoreSizeFound,"core Size found:",CoreSizeFound
		return matchFound,AllCoreSizeFound,countMatchFound,verificationReq

	'''For Apparel Category we need both straight match and Normalization Module'''
	if category.lower()=='apparel':
		#Rule: Normalize and straight match
		compAvailSize=preprocessNorm(compAvailSize)
		Norm_srcAvailSize={}
		with open("Normalized/nikedefaultdefault","r") as fin:
			for line in fin:
				line=line.strip()
				linelist=line.split(",")
				Norm_srcAvailSize[linelist[0]]=linelist
		compAvailSize_all=[]
		
		for size in compAvailSize:
			Norm_compAvailSize=[]
			Norm_keycompAvailSize=[]
			for key in Norm_srcAvailSize:
				for keySize in Norm_srcAvailSize[key]:
					# print " "+size.lower().strip()+" ", ", "+keySize.lower().strip()+" " ," "+keySize.lower().strip()+" " in " "+size.lower().strip()+" "
					if " "+keySize.lower().strip()+" " in " "+size.lower().strip()+" ":
						Norm_compAvailSize.append(keySize.lower().strip())
						Norm_keycompAvailSize.append(key.lower().strip())
			# print "Avail compkey:",Norm_keycompAvailSize
			# print "Avail compkeySize:",Norm_compAvailSize
			if len(Norm_keycompAvailSize)>0:
				key=''
				for num,eachkeySize in enumerate(Norm_compAvailSize):
					# print " "+size.lower().strip()+" "," "+eachkeySize.lower().strip()+" "," "+size.lower().strip()+" " == " "+eachkeySize.lower().strip()+" "
					if " "+size.lower().strip()+" " == " "+eachkeySize.lower().strip()+" ":
						key=Norm_keycompAvailSize[num]
						compAvailSize_all.append(key)
				# print "key",key		
				if key=='':
					for n1,eachkeySize in enumerate(Norm_compAvailSize):
						if eachkeySize in compAvailSize_all:
							continue
						maxList=[]
						for n2,eachkeySize2 in enumerate(Norm_compAvailSize):
							if " "+eachkeySize+" " in " "+eachkeySize2+" ":
								maxList.append(eachkeySize2)
						# print "eachkeySize,maxList", eachkeySize,maxList
						keySize=max(maxList,key=len)
						key=Norm_keycompAvailSize[Norm_compAvailSize.index(keySize.lower())]
						compAvailSize_all.append(key)
					# print "KeySize",keySize
					# print Norm_compAvailSize,Norm_keycompAvailSize
					# print "Key:",key
			# print compAvailSize_all
		CoreSizeFound=set(srcAvailSize).intersection(set(compAvailSize_all))
		compAvailSize_all=list(set(compAvailSize_all))
		if len(set(CoreSizeFound))==len(srcAvailSize):
			AllCoreSizeFound=True
		countMatchFound=len(set(CoreSizeFound))	
		print "src:",srcAvailSize,"comp:",compAvailSize,"found:",compAvailSize_all,"core Size found:",CoreSizeFound
	return matchFound,verificationReq,AllCoreSizeFound,countMatchFound

def main():	
	inputFile='test_nike_shoes.json'
	with open(inputFile,'r') as fin:
		for lnum,line in enumerate(fin):
			# if lnum>1000:
			# 	break
			data=json.loads(line)
			# try:
			# 	subcategory= data['subcategory']
			# except:
			# 	subcategory=''
			# if "Men's Shoes" not in subcategory:
			# 	continue
			attributes= json.loads(data['attributes'])
			# print attributes
			for num,i in enumerate(attributes):

				if "size" in ' '.join(i).lower():
					print i, "size" in ' '.join(i).lower(), i[1] 
					match("footwear","men","",str(i[1]))
	# match("apparel","default","","'small ', ' mens 3xl'")



if __name__ == '__main__':
	main()